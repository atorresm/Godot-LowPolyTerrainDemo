# Low-poly Terrain Demo

Simple low-poly terrain for Godot Engine 3.0 using Godot's PlaneMesh and a simple heightmap with a shader.

![screenshot](/terrain_screenshot.png)
